  

## CÀI ĐẶT MQTT SERVER 
### Cài đặt Mainflux
 **Requiment system**
 - ubuntu 18.04LTS 
 - RAM: 4GB, CPU: 2
 - DOCKER version 19.03.11,
 - DOCKER COMPOSE version 1.24.0 

### Step 1: Clone mainflux, CLI 


```
 $ git clone https://github.com/mainflux/mainflux.git
 $ wget https://github.com/mainflux/mainflux/releases/download/v0.11.0/mainflux-cli_v0.11.0_linux-amd64.tar.gz
 $ tar vzf mainflux-cli_v0.11.0_linux-amd64.tar.gz
 $ mv mainflux-cli cli
 ```


### step 2 : run project mainflux 

```
- $ cd mainflux/

- $ docker-compose -f docker/docker-compose.yml up -d 

```
### step 1: tạo user, password 
```
$ ./cli users create <email> <password> 
```
### step 2: lấy token xác thực cho user 

```
$ ./cli users token <email> <password>

```
### step 3: tạo things 

``` 

$ ./cli things create '{"name":"namethings"}' <token>
 
```
### step 4: tạo channels 

``` 

$ ./cli channels create '{"name":"name_channels"}' <token>
 
```



### step 5: lấy thông tin things 

``` 

$ ./cli things get all <token>
 
```
tương tự hình dưới: 
![Screen-Shot-2020-06-17-at-10.29.34b1d1b0993cc83b35.png](https://www.upsieutoc.com/images/2020/06/17/Screen-Shot-2020-06-17-at-10.29.34b1d1b0993cc83b35.png)


### step 6: lấy thông tin channels 

``` 

$ ./cli channels get all <token>
 
```
tương tự hình dưới: 
![Screen-Shot-2020-06-17-at-10.32.1403b1e602a26eac31.png](https://www.upsieutoc.com/images/2020/06/17/Screen-Shot-2020-06-17-at-10.32.1403b1e602a26eac31.png)

### step 7: Kết nối things và channels 

``` 

$ ./cli things connect THING_ID CHANNEL_ID <token>
 
```