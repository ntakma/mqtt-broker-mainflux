# KẾT NỐI CLIENT ĐẾN BROKER MQTT SERVER MAINFLUX
--------------------------------------------------------------------------

## Chuẩn bị môi trường 

**requirement env**
- python3.6 
``` pip install paho ```


## Sửa thông tin phù hợp với thông tin client.
### Điền thông tin vào file tokens.txt 

Sử dụng những thông tin token, things, channels đánh dấu bên trên và điền vào các trường sau: 
```
{"account_token": "<token>",
 "thing1_id": "<thing_id>",
   "channel_id": "<channels_id>",
    "thing1_key": "<thing_key>"
}
```


## Run project 

### Run file subcriber.py 

```

$ python subriber.py 

```
### Run file publisher.py 

```

$ python publisher.py 

```
